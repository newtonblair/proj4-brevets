import acp_times as acp_time

def test_final_checkpt():
	'''tests open and close times of a checkpoint right at the total brev distance point (end of race exactly)'''
	assert acp_time.open_time(200, 200, "2017-01-01 00:00") == "2017-01-01T05:53:00-08:00"
	assert acp_time.close_time(200, 200, "2017-01-01 00:00") == "2017-01-01T13:30:00-08:00"

def test_longer_final_checkpt():
	'''This tests a final checkpoint that is actually further than the 'overall' distance of the brevet'''
	assert acp_time.open_time(210, 200, "2017-01-01 00:00") == "2017-01-01T05:53:00-08:00"
	assert acp_time.close_time(210, 200, "2017-01-01 00:00") == "2017-01-01T13:30:00-08:00"

def test_leap_year():
	'''This tests to make sure that on a leap year the rollover from the 28th goes to the 29th'''
	assert acp_time.open_time(250, 400, "2020-02-28 20:00") == "2020-02-29T03:27:00-08:00"
	assert acp_time.close_time(250, 400, "2020-02-28 20:00") == "2020-02-29T12:40:00-08:00"

def test_start():
	'''This tests the open and close time of starting point'''
	assert acp_time.open_time(0, 400, "2017-01-01 05:00") == "2017-01-01T05:00:00-08:00"
	assert acp_time.close_time(0, 400, "2017-01-01 05:00") == "2017-01-01T06:00:00-08:00"

def test_new_year():
	'''This tests the transition into a new year'''
	assert acp_time.open_time(400, 600, "2018-12-31 00:00") == "2018-12-31T12:08:00-08:00"
	assert acp_time.close_time(400, 600, "2018-12-31 00:00") == "2019-01-01T02:40:00-08:00"